# Easel Driver for Docker

This repository contains a Dockerfile for building a docker container
that can run the easel-driver on any platform. This can be used, for example,
to run the Easel Driver from a Raspberry Pi if your CNC machine is far from
your work computer.

## Easy instructions for ArchLinux ARM on a Microcontroller (like Raspberry Pi), controlled by another computer

1. Set up docker on the device and ensure that it is working properly
2. Install the `easel-driver-docker package` from [the AUR](https://aur.archlinux.org/packages/easel-driver-docker-git/)
3. Plug your CNC into the device and turn it on
4. Use an SSH connection to the device to forward necessary ports:
```
ssh -L 1338:localhost:1338 -L 1438:localhost:1438 user@remote
```
5. Open Easel in your browser, it should be able to talk to your CNC device
6. Once you're done, turn off your CNC

## Easy instructions for other distributions on a Microcontroller (like Raspberry Pi), controlled by another computer

Note this requires systemd to work

1. Set up docker on the device and ensure that it is working properly
2. Clone this repository
3. Install the host tools (as root): `cd host && make install`
4. Plug your CNC into the device and turn it on
5. Use an SSH connection to the device to forward necessary ports:
```
ssh -L 1338:localhost:1338 -L 1438:localhost:1438 user@remote
```
6. Open Easel in your browser, it should be able to talk to your CNC device
7. Once you're done, turn off your CNC

## Easy instructions for ArchLinux, controlled locally

1. Set up docker and ensure it is working properly
2. Plug your CNC into the Pi and turn it on
3. Install the `easel-driver-docker package` from the AUR
4. Open Easel in your browser, it should be able to talk to your CNC device
5. Once you're done, turn off your CNC

## Longer explanation of this container and how to use it

The Dockerfile should work on any platform supported by `node:12-alpine`.
It is best to set up a UDev rule to always provide the same device name for your
CNC, especially if you use other devices on the same micro:

Here is an example `/etc/udev/rules.d/99-easel.rules` for a Carvey:
```
SUBSYSTEM=="tty", ATTRS{interface}=="Carvey", SYMLINK+="ttyCNC"
```

After reboot, whenever the Carvey is turned on, in addition to populating
a `/dev/ttyUSBn` entry, a symlink to it called `/dev/ttyCNC` will be created.

With the consistent naming, the container can be used to run easel driver *after*
plugging in and turning on your CNC mill:

```
docker run \
    --publish 1338:1338 \                     # Publish the ports Easel uses to talk to the driver
    --publish 1438:1438 \
    --volume /run/udev:/run/udev:ro \         # Easel driver needs to be able to read from UDev to discover serial ports
    --device /dev/ttyCNC:/dev/ttyUSB0 \    # Provide the CNC Serial device to the container
    --name easel-driver \
    --interactive \
    --tty \
    --rm \                                    # Remove container when it exits
    scallopedllama/easel-driver:latest
```

This will create and run a container in the forground running `easel-driver` with
access to the serial port for the CNC Mill. On the computer where you'll be using Easel,
use SSH port forwarding to forward the needed ports so Easel can connect to the driver:

```
ssh -L 1338:localhost:1338 -L 1438:localhost:1438 user@remote
```

When you're done, you can close the SSH port forward session and run `docker kill easel-driver`
in another terminal on the remote computer to stop the drive. It will clean up the docker
container automatically. The easel-driver does not respond properly to `SIGINT` for some reason,
so the `docker kill` command is unfortunately necessary.

Note that Docker's support for `--device` does *not* survive between device disconnects.
The Carvey, at least, removes its serial device when the Carvey is powered off so this
will not work for a long-term solution.

## Making this container automatically work

Using Udev with sytemd, it's possible to make it so the easel-driver is only running
while the CNC is connected and automatically stop the driver when it is disconnected.
This has the added benefit of working around the Docker `--device` issue.

Using the following modified Udev rule, udev will also tell systemd that this device
"wants" the `easel-driver` service.

```
SUBSYSTEM=="tty", ATTRS{interface}=="Carvey", SYMLINK+="ttyCNC", TAG+="systemd", ENV{SYSTEMD_WANTS}="easel-driver.service"
```

Additionally, make the following systemd unit at `/usr/lib/systemd/system/easel-driver.service`:

```
[Unit]
Description=Easel Driver
After=dev-ttyCNC.device docker.service
BindsTo=dev-ttyCNC.device

[Service]
TimeoutStartSec=0
ExecStartPre=-/usr/bin/docker stop %N
ExecStartPre=-/usr/bin/docker rm %N
ExecStartPre=-/usr/bin/docker pull scallopedllama/easel-driver:latest
ExecStart=-/usr/bin/docker run --publish 1338:1338  --publish 1438:1438 --volume /run/udev:/run/udev:ro --device /dev/ttyCNC:/dev/ttyUSB0 --name %N --rm scallopedllama/easel-driver:latest
ExecStop=/usr/bin/docker rm --force %N

[Install]
WantedBy=multi-user.target

```

*Note*: Make sure you include the `--rm` argument to the docker command in the systemd unit. Replace `dev-ttyCNC.device` to match whatever
your Udev rule adds as a symlink to the CNC serial port.

Reload and trigger Udev:

```
# udevadm control --reload
# udevadm trigger
```

Reload and enable the service:

```
# systemctl daemon-reload
# systemctl enable easel-driver
```

This systemd unit will automatically start a new docker container for the easel-driver every time the
CNC mill appears and automatically kill it once it disappears. This fixes the issue that the `--device`
argument will not survive disconnects and keeps the easel-driver running as little as possible.

### Deeper explanation of systmed unit

Devices in `/dev` automatically get a systemd device unit assigned to them, based on their name.
Regular units can use these devices as dependencies to, for example, ensure they are started after
the device appears (the `After=` line in the systemd unit).

Additionally, the `BindsTo=` line indicates that this unit should *only* be run *while* the subsequent
items are active. So in this case, systemd will only allow the container to run while the mill is connected.

These will cause systemd to stop the unit when the device is disconnected. Systemd also includes
some features in Udev which are controlled by environment variables to do additional things.
The Udev rule adds a condition to tell systemd that this device should start the `easel-driver.service`.

## Security improvements with Dockerized Easel Driver

In a typical use case, the Easel Driver is constantly running on your computer, waiting for a connection from
a websocket which it will then use to talk to Easel and control your CNC mill. This can be problematic for a
few reasons:

* The driver is run as root. If there are any RCE exploits in the code, a malicious site could connect to the
driver to do *anything* to your computer (it is root after all)
* Even if there is not an obvious RCE exploit, it *can* still control your CNC which could tell it to do dangerous
operations resulting in crashes, etc.
* The driver is always running. If there are any memory leaks in the driver, it's just sitting there getting worse and worse.

The Dockerized version fixes these issues:

* Though the driver still runs as root, it is in a docker container, which is harder to escape. If they do escape the docker
container, you're far less likely to have important things running on the device running this container. It's not perfect but it is better.
* The driver is only running while your CNC is actually on and you're actually using it
* Memory leaks will fix themselves with every power cycle of the CNC

## Things to do

* Set up pipeline to automatically build container for some platforms
* See about running driver as a user account
* Support more than just one CNC connected
    * Systemd unit can be made generic but ports will need to be specified in the Udev rule as well.

# Legal Notes

While the contents of this repository (Dockerfile, Udev rules, Systemd unit, etc.) are all licensed GPLv2,
I make no claim of ownership of the Easel Driver itself.

Easel and the Easel Driver are made by, copyright, etc. Inventables, Inc.

As it says in the GPLv2, I provide this tool to build a containerized Easel Driver to you

> "AS IS" WITHOUT WARRANTY

If something goes wrong with your CNC while using this software, it is entirely your own fault.
